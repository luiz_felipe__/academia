<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('modules.usuarios.form');
// });

Route::get('routes', function() {
    dd(Route::getRoutes());
});

Route::resource('auth', 'UsuarioController', ['except' => ['show']]);
Route::resource('cliente', 'ClienteController', ['except' => ['show']]);
Route::resource('exercicio', 'ExercicioController', ['except' => ['show']]);
Route::resource('musculo', 'MusculoController', ['except' => ['show']]);
Route::resource('serie', 'SerieController', ['except' => ['show']]);
Route::resource('cliente.ficha', 'Academia\FichaController');
Route::resource('cliente.medida', 'Academia\MedidaController', ['except' => ['show']]);


// Route::group(['prefix' => 'api'], function()
// {
// 	Route::group(['prefix' => 'academia'], function()
// 	{
// 		Route::get('', ['uses' => 'AcademiaController@allUsers'] );

// 		Route::get('{id}', ['uses' => 'AcademiaController@getUser'] );

// 		Route::post('', ['uses' => 'AcademiaController@saveUser'] );

// 		Route::put('{id}', ['uses' => 'AcademiaController@updateUser'] );

// 		Route::delete('{id}', ['uses' => 'AcademiaController@deleteUser'] );

// 	});

// });

Route::group(['prefix' => 'api'], function()
{
	Route::group(['prefix' => 'users'], function()
	{
		Route::get('', ['uses' => 'AcademiaController@allClientes'] );

		Route::get('{id}', ['uses' => 'AcademiaController@getCliente'] );

		Route::post('', ['uses' => 'AcademiaController@saveCliente'] );

		Route::put('{id}', ['uses' => 'AcademiaController@updateCliente'] );

		Route::delete('{id}', ['uses' => 'AcademiaController@deleteCliente'] );

	});

});



Auth::routes();

// Route::get('/home', 'HomeController@index');
