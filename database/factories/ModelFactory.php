<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Cliente::class, function (Faker\Generator $faker) {
    return [
    	'matricula_cliente' => $faker->numberBetween(1210, 1920),
        'nome_cliente' => $faker->name,
    ];
});

$factory->define(App\Exercicio::class, function (Faker\Generator $faker) {
    return [
        'nome_exercicio' => $faker->word,
        'alternativo_exercicio' => $faker->word,
    ];
});

$factory->define(App\Serie::class, function (Faker\Generator $faker) {
    return [
        'qtd_repeticoes_serie' => $faker->numberBetween(4, 20),
        'qtd_serie_serie' => $faker->numberBetween(1, 4),
    ];
});

$factory->define(App\Musculo::class, function (Faker\Generator $faker) {
    return [
        'nome_musculo' => $faker->word,
    ];
});
