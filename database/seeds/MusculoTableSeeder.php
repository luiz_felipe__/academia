<?php

use Illuminate\Database\Seeder;

class MusculoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Musculo::truncate();
    	
        factory(App\Musculo::class, 20)->create();
    }
}
