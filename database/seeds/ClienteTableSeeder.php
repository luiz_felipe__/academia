<?php

use Illuminate\Database\Seeder;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	App\Cliente::truncate();
    	
        factory(App\Cliente::class, 20)->create();
    }
}
