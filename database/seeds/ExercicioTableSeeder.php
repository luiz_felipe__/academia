<?php

use Illuminate\Database\Seeder;

class ExercicioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	App\Exercicio::truncate();
    	
        factory(App\Exercicio::class, 20)->create();
    }
}
