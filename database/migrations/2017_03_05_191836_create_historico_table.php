<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico', function (Blueprint $table) {
            $table->increments('id_historico');
            $table->integer('id_forekey_personal')->unsigned();
            $table->foreign('id_forekey_personal')->references('id_personal')->on('personais');
            $table->integer('id_forekey_cliente')->unsigned();
            $table->foreign('id_forekey_cliente')->references('id_cliente')->on('clientes');
            $table->integer('id_forekey_ficha')->unsigned();
            $table->foreign('id_forekey_ficha')->references('id_ficha')->on('fichas');
            $table->integer('id_forekey_medida')->unsigned();
            $table->foreign('id_forekey_medida')->references('id_medida')->on('medidas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historico');
    }
}
