<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medidas', function (Blueprint $table) {
            $table->increments('id_medida');
            $table->date('data_medicao_medida');
            $table->integer('peso_medida')->unsigned();
            $table->integer('peito_medida')->unsigned();
            $table->integer('ante_braco_medida')->unsigned();
            $table->integer('braco_medida')->unsigned();
            $table->integer('perna_medida')->unsigned();
            $table->integer('quadris_medida')->unsigned();
            $table->integer('panturrilha_medida')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medidas');
    }
}
