<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrupoMuscularTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_muscular', function (Blueprint $table) {
            $table->increments('id_grupo_muscular')->unsigned();
            $table->integer('id_forekey_exercicio')->unsigned();
            $table->foreign('id_forekey_exercicio')->references('id_exercicio')->on('exercicios');
            $table->integer('id_forekey_musculo')->unsigned();
            $table->foreign('id_forekey_musculo')->references('id_musculo')->on('musculos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grupo_muscular');
    }
}
