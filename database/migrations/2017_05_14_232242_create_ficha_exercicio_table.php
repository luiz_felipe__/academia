<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichaExercicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficha_exercicio', function (Blueprint $table) {
            $table->increments('id_ficha_exercicio');
            $table->integer('id_forekey_ficha')->unsigned();
            $table->foreign('id_forekey_ficha')->references('id_ficha')->on('fichas');
            $table->string('treino_ficha');
            $table->integer('id_forekey_exercicio')->unsigned();
            $table->foreign('id_forekey_exercicio')->references('id_exercicio')->on('exercicios');
            $table->integer('qtd_repeticoes_ficha');
            $table->integer('qtd_serie_ficha'); 
            $table->integer('tempo_descanco_ficha');
            $table->string('imagem_exercicio');
            $table->date('inicio');
            $table->date('termino');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Drop('ficha_exercicio');
    }
}
