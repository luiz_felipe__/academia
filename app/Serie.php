<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    protected $table = 'series';
    public $timestamps = false;
    protected $primaryKey = 'id_serie';
    protected $fillable = ['qtd_repeticoes_serie', 'qtd_serie_serie'];
}
