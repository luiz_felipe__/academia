<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercicio extends Model
{
    protected $table = 'exercicios';
    public $timestamps = false;
    protected $primaryKey = 'id_exercicio';
    protected $fillable = ['nome_exercicio', 'alternativo_exercicio'];

    public function fichas()
	{
	    return $this->belongsToMany('App\Exercicio','ficha_exercico', 'id_forekey_ficha', 'ficha_id');
	}
}
