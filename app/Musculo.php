<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Musculo extends Model
{
    protected $table = 'musculos';
    public $timestamps = false;
    protected $primaryKey = 'id_musculo';
    protected $fillable = ['nome_musculo'];

    
}
