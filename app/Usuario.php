<?php

namespace App;
use App\FichaExercicio;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;


class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';
    public $timestamps = false;
    protected $fillable = ['nome_usuario', 'senha'];

    public function allUsers()
    {
    	return self::all();
    }

    public function saveUser()
    {
    	$input = Input::all();
    	$user = new Usuario();
    	$user->fill($input);
    	$user->save();
    	return $user;
    }

    public function getUser($id)
    {
        $user = self::find($id);
        if(is_null($user))
        {
            return false;
        }
        return $user;
    }

    public function updateUser($id)
    {
        $user = self::find($id);
        if(is_null($user))
        {
            return false;    
        }
        $input = Input::all();
        $user->fill($input);
        $user->save();
        return $user;
    }

    public function deleteUser($id)
    {
        $user = self::find($id);
        if(is_null($user))
        {
            return false;
        }
        return $user->delete();
    }
}
