<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objetivo extends Model
{
    protected $table = 'objetivos';
    public $timestamps = false;
    protected $primaryKey = 'id_objetivo';
    protected $fillable = ['objetivo'];
}
