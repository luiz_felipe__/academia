<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    protected $table = 'fichas';
    protected $primaryKey = 'id_ficha';
    public $timestamps = false;
    protected $fillable = [ 
                            'id_forekey_cliente', 
    						'objetivo_ficha',
    						];

    public function cliente()
    {
        return $this->hasOne('App\Cliente', 'id_cliente', 'id_ficha');
    }

    public function exercicios()
    {
        return $this->belongsToMany('App\Ficha','ficha_exercicio', 'ficha_id','id_forekey_ficha');
    }   

}
