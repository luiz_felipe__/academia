<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treino extends Model
{
    protected $table = 'treinos';
    public $timestamps = false;
    protected $primaryKey = 'id_treino';
    protected $fillable = ['treino_tipo'];
}
