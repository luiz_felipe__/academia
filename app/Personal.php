<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $table = 'personais';
    public $timestamps = false;
    protected $fillable = ['nome', 'matricula'];
}
