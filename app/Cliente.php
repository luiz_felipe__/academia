<?php

namespace App;
use App\FichaExercicio;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $primaryKey = 'id_cliente';
    public $timestamps = false;
    protected $fillable = ['matricula_cliente', 'nome_cliente'];

    public function allClientes()
    {
        $array = [self::all(), FichaExercicio::all()];

        return $array;
    	//return ;
    }

    public function saveCliente()
    {
    	$input = Input::all();
        $cliente = new Cliente();
    	$cliente->fill($input);
    	$cliente->save();
    	return $cliente;
    }

    public function getCliente($id)
    {
        $cliente = self::find($id);

        if(is_null($cliente))
        {
            return false;
        }
        //$ficha = Ficha::find($cliente->id_cliente);
        $fichaExercicio = FichaExercicio::find($cliente->id_cliente);
        $data = [$cliente, $fichaExercicio];
        return $data;
    }

    public function updateCliente($id)
    {
        $Cliente = self::find($id);
        if(is_null($Cliente))
        {
            return false;    
        }
        $input = Input::all();
        $Cliente->fill($input);
        $Cliente->save();
        return $Cliente;
    }

    public function deleteCliente($id)
    {
        $Cliente = self::find($id);
        if(is_null($Cliente))
        {
            return false;
        }
        return $Cliente->delete();
    }
}
