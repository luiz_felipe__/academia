<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    protected $table = 'medidas';
    protected $primaryKey = 'id_medida';
    public $timestamps = false;
    protected $fillable = ['data_medicao_medida', 'peso_medida', 'peito_medida', 'ante_braco_medida', 
    						'braco_medida', 'perna_medida', 'quadris_medida', 'panturrilha_medida'];
}
