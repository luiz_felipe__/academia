<?php

namespace App\Http\Controllers;
use App\Musculo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class MusculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if(Input::has('musculo_search') == false){
            $musculos = DB::table('musculos')->orderBy('nome_musculo')->paginate(5);
        }else{
            $name = $request->musculo_search;
            $musculos = DB::table('musculos')->where('nome_musculo', 'like', '%'.$name.'%')->orderBy('nome_musculo')->paginate(8);
        }

        $data = [
            'data' => $musculos
        ];

        return view('modules.musculos.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.musculos.form')
            ->with('type', 'create')
            ->with('action', route('musculo.store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->musculo;
            $musculo = new Musculo($data);
            $musculo->save();    

            return redirect()
                ->route('musculo.index')
                ->with(['success' => 'musculo cadastrado com sucesso!']);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['Não foi possível cadastrar o musculo, tente novamente mais tarde!<br>' . $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $musculo = Musculo::find($id);

        if (is_null($musculo)) {
            throw new NotFoundHttpException;
        }

        return view('modules.musculos.form')
            ->with('musculo', $musculo)
            ->with('type', 'edit')
            ->with('action', route('musculo.update', $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $musculo = Musculo::find($id);
            $musculo->fill($request->musculo)->save();

            return redirect()
                ->route('musculo.index')
                ->with(array('success' => 'musculo editado com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('musculo.index')
                ->withErrors(['Não foi possível cadastrar o musculo no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $musculo = Musculo::find($id);
            if (is_null($musculo)) {
                abort(404);
            }

            $musculo->delete();

            return redirect()
                ->route('musculo.index')
                ->with(array('success' => 'musculo excluido com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('musculo.index')
                ->withErrors(['Não foi possível deletar o musculo no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }
}
