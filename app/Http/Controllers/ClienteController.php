<?php

namespace App\Http\Controllers;
use App\Cliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // protected $messages = [
    //     'cliente.nome_cliente.max:10' => 'O campo nome não pode ter mais de 45 caracteres',
    //     'cliente.matricula_cliente.max:6' => 'A matricula so pode ter no máximo 6 dígitos',
    // ];

    public function index(Request $request)
    {
        if(Input::has('cliente_search') == false){
            $clientes = DB::table('clientes')->orderBy('nome_cliente')->paginate(5);
        }else{
            $name = $request->cliente_search;
            $clientes = DB::table('clientes')->where('nome_cliente', 'like', '%'.$name.'%')->orderBy('nome_cliente')->paginate(8);
        }

        $data = [
            'data' => $clientes
        ];

        return view('modules.clientes.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.clientes.form')
            ->with('type', 'create')
            ->with('action', route('cliente.store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $request)
    {
        $matricula = (int)$request->cliente['matricula_cliente'];
        $data = (boolean)DB::table('clientes')->where('matricula_cliente', $matricula)->first();
       
       if($data)
       {
           return redirect()
               ->back()
               ->withInput()
               ->withErrors(['Este cliente já foi cadastrado!<br>']);
       }

       try {
           $data = $request->cliente;
           $data['matricula_cliente'] = (int)$data['matricula_cliente'];
           $cliente = new Cliente($data);
           $cliente->save();    

           return redirect()
               ->route('cliente.index')
               ->with(['success' => 'cliente cadastrado com sucesso!']);
       } catch (\Exception $e) {
           return redirect()
               ->back()
               ->withInput()
               ->withErrors(['Não foi possível cadastrar o cliente, tente novamente mais tarde!<br>' . $e->getMessage()]);
       } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);

        if (is_null($cliente)) {
            throw new NotFoundHttpException;
        }

        return view('modules.clientes.form')
            ->with('cliente', $cliente)
            ->with('type', 'edit')
            ->with('action', route('cliente.update', $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cliente = Cliente::find($id);
            $cliente->fill($request->cliente)->save();

            return redirect()
                ->route('cliente.index')
                ->with(array('success' => 'cliente editado com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('cliente.index')
                ->withErrors(['Não foi possível cadastrar o cliente no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cliente = Cliente::find($id);
            if (is_null($cliente)) {
                abort(404);
            }

            $cliente->delete();

            return redirect()
                ->route('cliente.index')
                ->with(array('success' => 'cliente excluido com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('cliente.index')
                ->withErrors(['Não foi possível deletar o cliente no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }
}
