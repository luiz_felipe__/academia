<?php

namespace App\Http\Controllers;
use App\Personal;
use Exception;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Personal::all();
        $data = [
            'rows' => $rows
        ];

        return view('modules.personais.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.personais.form')
            ->with('type', 'create')
            ->with('action', route('personal.store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->personal;
            $data['matricula'] = (int)$data['matricula'];
            $personal = new Personal($data);
            $personal->save();    

            return redirect()
                ->route('personal.index')
                ->with(['success' => 'Personal cadastrado com sucesso!']);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['Não foi possível cadastraro personal, tente novamente mais tarde!<br>' . $e->getMessage()]);
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $personal = Personal::find($id);

        if (is_null($personal)) {
            throw new NotFoundHttpException;
        }

        return view('modules.personais.form')
            ->with('personal', $personal)
            ->with('type', 'edit')
            ->with('action', route('personal.update', $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $personal = Personal::find($id);
            $personal->fill($request->personal)->save();

            return redirect()
                ->route('personal.index')
                ->with(array('success' => 'Personal editado com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('personal.index')
                ->withErrors(['Não foi possível cadastrar o personal no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $personal = Personal::find($id);
            if (is_null($personal)) {
                abort(404);
            }

            $personal->delete();

            return redirect()
                ->route('personal.index')
                ->with(array('success' => 'Personal excluido com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('personal.index')
                ->withErrors(['Não foi possível deletar o personal no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }
}
