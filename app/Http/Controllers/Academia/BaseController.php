<?php

namespace App\Http\Controllers\Academia;

use App\Cliente;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    protected $cliente;
    

    /**
     * BaseController constructor.
     * @param Request $Request
     */
    public function __construct(Request $request)
    {
        $this->cliente = Cliente::find($request->cliente);
        if (is_null($this->cliente))
            abort(404);

        // Share data in all views
        View::share('cliente', $this->cliente);
    }
}
