<?php

namespace App\Http\Controllers\academia;
use App\Medida;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class MedidaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Input::has('medida_search') == false){
            $medidas = DB::table('medidas')->paginate(5);
        }else{
            $id = $request->medida_search;
            $medidas = DB::table('medidas')->where('id_medida', 'like', '%'.$id.'%')->paginate(8);
        }

        //$medidas = Medida::all();
        $data = [
            'data' => $medidas
        ];

        return view('modules.medidas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.medidas.form')
            ->with('type', 'create')
            ->with('action', route('cliente.medida.store', $this->cliente->id_cliente));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function convertFotInt($data) {
        $dataInt = [];
        
        foreach ($data as $key => $value) {
            if(strlen($value) <= 3){
                $dataInt[$key] = (int)$value;
            }
            else
            {
                $dataInt[$key]  = $value;   
            }
        }
        return $dataInt; 
    }

    public function store(Request $request)
    {
        $data = $request->Medida;
        $medidas = $this->convertFotInt($data);
        
        try {
            $medida = new Medida($medidas);
            $medida->save();    

            return redirect()
                ->route('cliente.medida.index', $this->cliente->id_cliente)
                ->with(['success' => 'medidas cadastradas com sucesso!']);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['Não foi possível cadastrar as medidas, tente novamente mais tarde!<br>' . $e->getMessage()]);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cliente, $id)
    {
        $medida = Medida::find($id);

        if (is_null($medida)) {
            throw new NotFoundHttpException;
        }

        return view('modules.medidas.form')
            ->with('medida', $medida)
            ->with('type', 'edit')
            ->with('action', route('cliente.medida.update', [$this->cliente->id_cliente, $id]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cliente, $id)
    {
        $data = $request->Medida;
        $medidas = $this->convertFotInt($data);

        try {
            Medida::find($id)->update($medidas);
            return redirect()
                ->route('cliente.medida.index', [$this->cliente->id_cliente, $id])
                ->with(array('success' => 'medidas editadas com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('cliente.medida.index', [$this->cliente->id_cliente, $id])
                ->withErrors(['Não foi possível cadastrar as medidas no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
