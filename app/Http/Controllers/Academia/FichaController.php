<?php

namespace App\Http\Controllers\academia;
use App\Ficha;
use App\Cliente;
use App\Exercicio;
use App\Musculo;
use App\FichaExercicio;
use App\Treino;
use App\Objetivo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class FichaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$row = FichaExercicio::All();
        if(Input::has('ficha_search') == false){
            $clientes = DB::table('ficha_exercicio')->paginate(5);
        }else{
            $id = (int)$request->ficha_search;
            $clientes = DB::table('ficha_exercicio')->where('id_forekey_ficha', 'like', '%'.$id.'%')->paginate(8);
        }

        $data = [
            'data' => $clientes
        ];

        return view('modules.fichas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $exercicios = Exercicio::all();
       $treinos = Treino::all();
       $objetivos = Objetivo::all();
       
       return view('modules.fichas.form')
            ->with('type', 'create')
            ->with('exercicios', $exercicios)
            ->with('treinos', $treinos)
            ->with('objetivos', $objetivos)
            ->with('cliente', $this->cliente)
            ->with('action', route('cliente.ficha.store', $this->cliente->id_cliente));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    private function convertForInt($data)
    {
        $dataInt = [];

        foreach ($data as $chaves => $itens)
        {
            foreach ($itens as $key => $item)
            {
                if (ctype_digit($item))
                {
                    $dataInt[] = [
                        'chave' => $chaves,
                        'valor' => (int)$item
                    ];    
                }
                else
                {
                    $dataInt[] = [
                        'chave' => $chaves,
                        'valor' => $item
                    ];
                }
                
            }
        }
        
        return $dataInt; 
    }


    public function store(Request $request)
    {
        try {

                $dataFicha = $request->Objetivo;
                $dataFicha['id_forekey_cliente'] = $this->cliente->id_cliente;    
                $ficha = new Ficha($dataFicha);
                $ficha->save();

                $exercicio = $request->Exercicio;
                $repeticao = $request->Repeticao;
                $serie = $request->Serie;
                $descanso = $request->Descanso;
                $imagem = $request->Image;
                $dataInicio = $request->dataInicio['inicio'];
                $dataTermino = $request->dataTermino['termino'];
                $data = [];

                foreach ($exercicio as $chaves => $itens)
                {
                    foreach ($itens as $chave => $item)
                    {
                        $data['treino_ficha'] = (string)$chaves;
                        $data['id_forekey_ficha'] = (int)$ficha->id_ficha;
                        $data['id_forekey_exercicio'] = (int)$exercicio[$chaves][$chave];  
                        $data['qtd_repeticoes_ficha'] = (int)$repeticao[$chaves][$chave];
                        $data['qtd_serie_ficha'] = (int)$serie[$chaves][$chave];  
                        $data['tempo_descanco_ficha'] = (int)$descanso[$chaves][$chave];
                        $data['imagem_exercicio'] = (string)$imagem[$chaves][$chave];
                        $data['inicio'] = $dataInicio;
                        $data['termino'] = $dataTermino;

                        $fichaExercicio = new FichaExercicio($data);                             
                        $fichaExercicio->save();
                    }
                }
           
            return redirect()   
                ->route('cliente.ficha.index', $this->cliente->id_cliente)
                ->with(['success' => 'fichas cadastradas com sucesso!']);
        }
        catch (\Exception $e)
        {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['Não foi possível cadastrar as fichas, tente novamente mais tarde!<br>' . $e->getMessage()]);
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cliente, $id)
    {
        $fichaExercicio = FichaExercicio::find($id);
        $objetivo_ficha = Ficha::find($fichaExercicio->id_forekey_ficha)->objetivo_ficha;

        $exercicios = Exercicio::all();
        $objetivos = Objetivo::all();
        $treinos= Treino::all();

        if (is_null($fichaExercicio)) {
            throw new NotFoundHttpException;
        }

        return view('modules.fichas.formEdit')
            ->with('fichaExercicio', $fichaExercicio)
            ->with('exercicios', $exercicios)
            ->with('objetivos', $objetivos)
            ->with('objetivo_ficha', $objetivo_ficha)
            ->with('treinos', $treinos)
            ->with('type', 'edit')
            ->with('action', route('cliente.ficha.update', [$this->cliente->id_cliente, $id] ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cliente, $id)
    {
        $data = $request->Medida;
        $medidas = $this->convertFotInt($data);

        try {
            FichaExercicio::find($id)->update($medidas);
            return redirect()
                ->route('cliente.medida.index', [$this->cliente->id_cliente, $id])
                ->with(array('success' => 'medidas editadas com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('cliente.medida.index', [$this->cliente->id_cliente, $id])
                ->withErrors(['Não foi possível cadastrar as medidas no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
