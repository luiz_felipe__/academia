<?php

namespace App\Http\Controllers;
use App\Grupo_muscular;
use App\Exercicio;
use App\Musculo;
use Illuminate\Http\Request;

class Grupo_muscularController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Grupo_muscular::exercicios();
        
        $data = [
            'rows' => $rows
        ];

        return view('modules.grupo_muscular.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gp = Grupo_muscular::all();
        $exer = Exercicio::all();
        $musc = Musculo::all();

        $data = [
            'gp' => $grupo_muscular,
            'exer' => $exer,
            'musc' => $musc
        ];

        return view('modules.grupo_muscular.form', $data)
            ->with('type', 'create')
            ->with('action', route('grupo_muscular.store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->grupo_muscular;
            $grupo_muscular = new Grupo_muscular($data);
            $grupo_muscular->save();    

            return redirect()
                ->route('grupo_muscular.index')
                ->with(['success' => 'grupo_muscular cadastrado com sucesso!']);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['Não foi possível cadastrar o grupo_muscular, tente novamente mais tarde!<br>' . $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grupo_muscular = Grupo_muscular::find($id);

        if (is_null($grupo_muscular)) {
            throw new NotFoundHttpException;
        }

        return view('modules.grupo_musculars.form')
            ->with('grupo_muscular', $grupo_muscular)
            ->with('type', 'edit')
            ->with('action', route('grupo_muscular.update', $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $grupo_muscular = Grupo_muscular::find($id);
            $grupo_muscular->fill($request->grupo_muscular)->save();

            return redirect()
                ->route('grupo_muscular.index')
                ->with(array('success' => 'grupo_muscular editado com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('grupo_muscular.index')
                ->withErrors(['Não foi possível cadastrar o grupo_muscular no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $grupo_muscular = Grupo_muscular::find($id);
            if (is_null($grupo_muscular)) {
                abort(404);
            }

            $grupo_muscular->delete();

            return redirect()
                ->route('grupo_muscular.index')
                ->with(array('success' => 'grupo_muscular excluido com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('grupo_muscular.index')
                ->withErrors(['Não foi possível deletar o grupo_muscular no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }
}
