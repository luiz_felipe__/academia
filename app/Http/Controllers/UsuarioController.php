<?php

namespace App\Http\Controllers;
use App\Usuario;
use App\Cliente;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("modules.usuarios.form");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientes = Cliente::paginate(5);
        $senha = $request->Usuario["senha"];
        
        $user = DB::table('usuarios')->where('senha', $senha)->first();
        $data = [
            'data' => $clientes
        ];

        if( (boolean)$user )
        {
            return view('modules.clientes.index', $data);            
        }
        else
        {
            return redirect()
               ->back()
               ->withInput()
               ->withErrors(['Este usuario não consta nos registros!<br>']);    
        }   

        // try {
        //    $usuario = new usuario($request->Usuario);
        //    $usuario->save();    

        //    return redirect()
        //        ->route('modules.clientes.index', $data)
        //        ->with(['success' => 'usuario cadastrado com sucesso!']);
        // } catch (\Exception $e) {
        //    return redirect()
        //        ->back()
        //        ->withInput()
        //        ->withErrors(['Não foi possível cadastrar o usuario, tente novamente mais tarde!<br>' . $e->getMessage()]);
        // }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
