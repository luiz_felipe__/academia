<?php

namespace App\Http\Controllers;
use App\Exercicio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ExercicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Input::has('exercicio_search') == false){
            $exercicios = DB::table('exercicios')->orderBy('nome_exercicio')->paginate(5);
        }else{
            $name = $request->exercicio_search;
            $exercicios = DB::table('exercicios')->where('nome_exercicio', 'like', '%'.$name.'%')->orderBy('nome_exercicio')->paginate(8);
        }

        $data = [
            'data' => $exercicios
        ];

        return view('modules.exercicios.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.exercicios.form')
            ->with('type', 'create')
            ->with('action', route('exercicio.store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->exercicio;
            $exercicio = new Exercicio($data);
            $exercicio->save();    

            return redirect()
                ->route('exercicio.index')
                ->with(['success' => 'exercicio cadastrado com sucesso!']);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['Não foi possível cadastrar o exercicio, tente novamente mais tarde!<br>' . $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exercicio = Exercicio::find($id);

        if (is_null($exercicio)) {
            throw new NotFoundHttpException;
        }

        return view('modules.exercicios.form')
            ->with('exercicio', $exercicio)
            ->with('type', 'edit')
            ->with('action', route('exercicio.update', $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $exercicio = Exercicio::find($id);
            $exercicio->fill($request->exercicio)->save();

            return redirect()
                ->route('exercicio.index')
                ->with(array('success' => 'exercicio editado com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('exercicio.index')
                ->withErrors(['Não foi possível cadastrar o exercicio no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $exercicio = Exercicio::find($id);
            if (is_null($exercicio)) {
                abort(404);
            }

            $exercicio->delete();

            return redirect()
                ->route('exercicio.index')
                ->with(array('success' => 'exercicio excluido com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('exercicio.index')
                ->withErrors(['Não foi possível deletar o exercicio no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }
}
