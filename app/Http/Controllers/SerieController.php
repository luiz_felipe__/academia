<?php

namespace App\Http\Controllers;
use App\Serie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          if(Input::has('serie_search') == false){
            $series = DB::table('series')->orderBy('qtd_repeticoes_serie')->paginate(5);
        }else{
            $busca = $request->serie_search;
            $series = DB::table('series')->where('qtd_repeticoes_serie', 'like', '%'.$busca.'%')->orderBy('qtd_repeticoes_serie')->paginate(8);
        }

        $data = [
            'rows' => $series
        ];

        return view('modules.series.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.series.form')
            ->with('type', 'create')
            ->with('action', route('serie.store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->serie;
            $serie = new serie($data);
            $serie->save();    

            return redirect()
                ->route('serie.index')
                ->with(['success' => 'serie cadastrada com sucesso!']);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['Não foi possível cadastrar a serie, tente novamente mais tarde!<br>' . $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serie = Serie::find($id);

        if (is_null($serie)) {
            throw new NotFoundHttpException;
        }

        return view('modules.series.form')
            ->with('serie', $serie)
            ->with('type', 'edit')
            ->with('action', route('serie.update', $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $serie = Serie::find($id);
            $serie->fill($request->serie)->save();

            return redirect()
                ->route('serie.index')
                ->with(array('success' => 'serie editada com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('serie.index')
                ->withErrors(['Não foi possível cadastrar a serie no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $serie = Serie::find($id);
            if (is_null($serie)) {
                abort(404);
            }

            $serie->delete();

            return redirect()
                ->route('serie.index')
                ->with(array('success' => 'serie excluida com sucesso!'));
        } catch (\Exception $e) {
            return redirect()
                ->route('serie.index')
                ->withErrors(['Não foi possível deletar a serie no momento, tente novamente mais tarde!' . $e->getMessage()])
                ->withInput();
        }
    }
}
