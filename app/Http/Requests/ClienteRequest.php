<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'cliente.nome_cliente' => 'required',
             'cliente.matricula_cliente' => 'required|max:4',
            
        ];
    }

    public function messages()
    {
        return [
             'cliente.nome_cliente.required' => 'O nome do cliente é requirido',
             'cliente.matricula_cliente.required' => 'A matricula do cliente é requirida',
             'cliente.matricula_cliente.max' => 'A matricula so pode ter no máximo 4 dígitos',  
        ];
    }
}
