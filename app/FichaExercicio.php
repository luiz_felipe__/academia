<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FichaExercicio extends Model
{
    protected $table = 'ficha_exercicio';
    protected $primaryKey = 'id_ficha_exercicio';
    public $timestamps = false;
    protected $fillable = [ 
                            'id_forekey_ficha', 
    						'treino_ficha',
    						'id_forekey_exercicio',
    						'qtd_repeticoes_ficha',
    						'qtd_serie_ficha',
                            'tempo_descanco_ficha',
                            'imagem_exercicio',
                            'inicio',
                            'termino'
    					];
}
