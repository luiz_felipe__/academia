<div class="ui secondary vertical pointing menu" id="steps">
    <div class="ui accordion">
        <div class="title">
            <a class="item" href="{{route('cliente.index')}}">
                Cliente <i class="user icon"></i>
            </a>
            <a class="item" href="{{route('exercicio.index')}}">
                Exercicio <i class="child icon"></i>
            </a>
        </div> 
    </div>
</div>