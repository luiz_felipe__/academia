@extends('layouts.app')
<div class="ui container">
	    
	    <div id="wrapper">
	        <div class="ui container">
	            @yield('content')
	        </div>
	    </div>   
	</div>
