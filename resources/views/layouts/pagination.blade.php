<?php $totalPages = ceil($data->total()/$data->perPage()); ?>
@if ($totalPages > 1)
    <div class="ui pagination menu">
        <a class="icon item {{$data->currentPage() != 1 ? null : 'disabled'}}" href="{{$data->currentPage() != 1 ? $data->previousPageUrl() : 'javascript:;'}}">
            <i class="left chevron icon"></i>
        </a>

        @for ($i = $data->currentPage()-3; $i < $data->currentPage()+3; $i++)
            @continue($i <= 0 || $i > $totalPages)
            <a class="item {{$data->currentPage() == $i ? 'active' : null}}" href="{{$data->currentPage() == $i ? 'javascript:;' : $data->url($i)}}">{{$i}}</a>
        @endfor

        <a class="icon item {{$data->currentPage() != $totalPages ? null : 'disabled'}}" href="{{$data->currentPage() != $totalPages ? $data->nextPageUrl() : 'javascript:;'}}">
            <i class="right chevron icon"></i>
        </a>
    </div>
@endif