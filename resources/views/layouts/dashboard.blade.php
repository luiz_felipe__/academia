@extends('layouts.app')
@section('content')
@extends('layouts.navbar')

    <div class="ui grid">
        <div class="four wide column">
            @include('layouts.steps')
        </div>
        <div class="twelve wide column" id="dashboard">
            @include('layouts.alerts')
            @yield('content-wizard')
        </div>
    </div>
@endsection