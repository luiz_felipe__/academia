<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Workout Tracker') }}</title>
    

    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="image_src" type="image/jpeg" href="/images/logo.png" />
    <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <!-- Site Properities -->
    <meta name="generator" content="Visual Studio 2015" />
    <title>Header | Golgi Admin</title>
    <meta name="description" content="Golgi Admin Theme" />
    <meta name="keywords" content="html5, ,semantic,ui, library, framework, javascript,jquery,admin,theme" />
    <link href="{{asset('assets/dist/semantic.min.css')}}" rel="stylesheet" />
    <!-- <link href="{{asset('assets/css/main.min.css')}}" rel="stylesheet" /> -->
    <!-- <link href="{{asset('assets/plugins/pacejs/pace.css')}}" rel="stylesheet" /> -->
    <script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">


    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="wrapper">
            @yield('content')
    </div>   
</div>

    <!-- Scripts -->
    <!-- <script src="{{asset('assets/plugins/nicescrool/jquery.nicescroll.min.js')}}"></script> -->
    <script src="{{asset('assets/dist/semantic.min.js')}}"></script>
    <!-- <script data-pace-options='{ "ajax": false }' src="{{asset('assets/plugins/pacejs/pace.js')}}"></script> -->
    <!-- <script src="{{asset('assets/js/app.min.js')}}"></script> -->
    <script src="{{asset('assets/js/jquery.mask.min.js')}}"></script>
    <script src="{{asset('assets/js/script.js')}}"></script>
</body>
</html>
