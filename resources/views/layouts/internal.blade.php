@extends('layouts.app')
@section('content')
@extends('layouts.navbar')

    <div class="ui grid">
        <div class="four wide column" >
            @include('layouts.internalsteps')
        </div>
        <div class="twelve wide column" id="internalsteps">
            @include('layouts.alerts')
            @yield('content-wizard-internal')
        </div>
    </div>
@endsection