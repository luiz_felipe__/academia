@if (count($errors) > 0)
    <div class="ui error message">
        <i class="close icon"></i>
        <div class="header">Erro!</div>
        <ul class="list">
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (Session::get('success'))
    <div class="ui success message">
        <i class="close icon"></i>
        <div class="header">Sucesso!</div>
        <p>{!! Session::get('success') !!}</p>
    </div>
@endif
