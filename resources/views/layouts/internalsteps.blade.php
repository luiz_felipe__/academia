<div class="ui secondary vertical pointing menu">
    <div class="ui accordion">
        <div class="title">
            <a class="item" href="{{route('cliente.ficha.index', $cliente->id_cliente)}}">
                Fichas <i class="ordered list icon"></i>
            </a>
            <a class="item" href="{{route('cliente.medida.index', $cliente->id_cliente)}}">
                Medidas <i class="text height icon"></i>
            </a>
        </div>
    </div>
</div>
