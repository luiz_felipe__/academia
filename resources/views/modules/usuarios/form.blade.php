@extends('layouts.app')

@section('content')
@include('layouts.alerts')

<div class="signin">
    <form action="{{route('auth.store')}}" method="post" enctype="multipart/form-data" class="ui large form">
        {!! csrf_field() !!}
        <div class="ui container">
            <div class="ui equal width equal height center aligned padded grid stackable">
                <div class="row">
                    <div class="five wide column">
                        <div class="ui segments">
                            <div class="ui segment inverted nightli">
                                <h3 class="ui header">Login</h3>
                            </div>
                            <div class="ui segment">
                                <div class="ui divider"></div>
                                <div class="ui input fluid">
                                    <label style="float: left; align-self: center; margin-right: 10px; display: inline-block; vertical-align: baseline;">Nome de Usuário:</label>
                                    <input id="nome" type="text" name="Usuario[nome_usuario]" required placeholder="Nome">
                                </div>
                                <div class="ui divider hidden"></div>
                                <div class="ui input fluid">
                                    <label style="float: left; align-self: center; margin-right: 10px; display: inline-block; vertical-align: baseline;">Senha:</label>
                                    <input id="password" type="password" name="Usuario[senha]" required placeholder="Senha">
                                </div>
                                <div class="ui divider hidden"></div>

                                <div class="g-recaptcha" data-sitekey="6LeR3xkTAAAAAKFv-HT60HvhrqHGMfQYHup1JFI2"></div>
                                <div class="ui divider hidden"></div>
                                <button class="ui primary fluid button"><i class="key icon"></i>Entrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>            

@endsection