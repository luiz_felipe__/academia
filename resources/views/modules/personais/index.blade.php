@extends('modules.personais.module')

@section('module-content')

    <div class="container search">
        <form>
            <div class="ui segment">
                <h4 class="ui header">Termo de pesquisa</h4>
                    <div class="ui input">
                        <input type="text" placeholder="Search...">
                    </div>
                    <button class="ui blue button"><i class="search icon"></i></button>
            </div>
        </form>
    </div>
    
    @if ($rows)
        <div class="ui grid">
            <div class="twelve wide column">
                 <table class="ui table">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Matricula</th>
                        <th width="160">Ações</td>
                    </tr>
                    </thead>
               
                    <tbody>
                        @foreach($rows as $row)
                        <tr>
                            <td>
                                <div>{{$row->nome}}</div>
                            </td>
                            <td>
                                <div>{{$row->matricula}}</div>
                            </td>
                            <td>
                                <form action="{{route('personal.destroy', $row->id)}}" method="post" class="ui form" onsubmit="return deleteData();">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    
                                    <a href="{{route('personal.edit', $row->id ) }}" class="ui blue icon button" data-tooltip="Editar"><i class="icon edit"></i></a>
                                    <button class="ui red icon button btn-remove" data-tooltip="Remover"><i class="icon trash"></i></button>
                                </form>
                            </td>
                        </tr>
                       @endforeach    
                    </tbody>
                </table>
            </div>
        </div>

    @else
        <p>Nenhum registro encontrado.</p>
    @endif

@endsection