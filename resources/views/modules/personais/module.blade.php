@extends('layouts.dashboard')

@section('content-wizard')

    <div class="action-buttons">
        <a href="{{route('personal.index')}}" class="ui button">Listar personal</a>
        <a href="{{route('personal.create')}}" class="ui button">Novo personal</a>  
    </div>
    
     @yield('module-content')

@endsection