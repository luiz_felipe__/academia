@extends('modules.personais.module')

@section('module-content')

    <div class="container">
        <form action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif

            <div class="ui segment">
                <h4 class="ui header">Cadastrar Personal</h4>
                <div class="field">
                    <div class="two fields">
                        <div class="field">
                            <label for="personal_name">Name:</label>
                            <input type="text" id="personal_name" name="personal[nome]" value="{{($type == 'create' ? old('personal.nome') : $personal->nome)}}" placeholder="First Name"  required>
                        </div>
                        <div class="field">
                            <label for="personal_matricula">Matricula:</label>
                            <input type="text" id="personal_matricula" name="personal[matricula]" value="{{($type == 'create' ? old('personal.matricula') : $personal->matricula)}}" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="field">
                        <button type="submit" class="ui green button" name="save">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection