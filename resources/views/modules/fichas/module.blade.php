@extends('layouts.internal')

@section('content-wizard-internal')

    <div class="container">
        <a href="{{ route('cliente.ficha.index', $cliente->id_cliente) }}" class="ui button teal">Listar Fichas</a>
        <a href="{{ route('cliente.ficha.create', $cliente->id_cliente) }}" class="ui button teal">Nova Ficha</a>
        @yield('module-content')
    </div>

@endsection