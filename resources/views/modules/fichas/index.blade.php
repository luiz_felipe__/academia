@extends('modules.fichas.module')

@section('module-content')
    
    <div class="container search">
        <form action="{{route('cliente.ficha.index', $cliente->id)}}" method="get" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}
            <div class="ui icon input">
              <input type="text" nmae="ficha_search" placeholder="Search...">
              <i class="circular search link icon"></i>
            </div>
        </form>
    </div>   

    @if (!is_null($data))
    
            <div class="twelve wide column">
                <table class="ui single line celled table">
                    <thead>
                    <tr>
                        <th>Ficha</th>
                        <th>Treino</th>
                        <th >Exercicio</th>
                        <th>Repetição</th>
                        <th >Serie</th>
                        <th>descanso</th>
                        <th >Image</th>
                        <th>Data inicial</th>
                        <th>Data final</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach ($data as $row)
                        <tr>
                            <td>
                                <div>{{ $row->id_forekey_ficha }}</div>
                            </td>
                            <td>
                                <div>{{ $row->treino_ficha }}</div>
                            </td>
                            <td>
                                <div>{{ $row->id_forekey_exercicio }}</div>
                            </td>
                            <td>
                                <div>{{ $row->qtd_repeticoes_ficha }}</div>
                            </td>
                            <td>
                                <div>{{ $row->qtd_serie_ficha }}</div>
                            </td>
                            <td>
                                <div>{{ $row->tempo_descanco_ficha }}</div>
                            </td>
                            <td>
                                <div>{{ $row->imagem_exercicio }}</div>
                            </td>
                            <td>
                                <div>{{ $row->inicio }}</div>
                            </td>
                            <td>
                                <div>{{ $row->termino }}</div>
                            </td>

                            <td>
                                <a href="{{route('cliente.ficha.edit', [ $cliente->id_cliente, $row->id_ficha_exercicio ]) }}" class="ui blue icon button" data-tooltip="Editar">
                                    <i class="icon edit"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <div>
                @include('layouts.pagination')
            </div>
    @else
        <p>Nenhum registro encontrado.</p>
    @endif

@endsection