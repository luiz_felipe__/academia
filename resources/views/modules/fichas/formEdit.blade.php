@extends('modules.fichas.module')

@section('module-content')

    <div class="ui container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
    
            <div class="ui segment">    
                <h4 class="ui header">Criar ficha de treino</h4>
                <div class="two fields">
                    <div class="field">
                        <label for="">Data de início da ficha</label>
                        <input type="date" name="dataInicio[inicio]" id="ficha_name" value="{{ $fichaExercicio->inicio }}">
                    </div>
                    <div class="field">
                        <label for="">Data de término da ficha</label>
                        <input type="date" name="dataTermino[termino]" id="ficha_name" value="{{ $fichaExercicio->termino }}">
                    </div>
                </div>
                <br>     
                
                <div class="two fields">
                        <div class="field">
                            <label for="ficha_objetivo">Objetivo:</label>
                            <select name="Objetivo[objetivo_ficha]" class="ui dropdown">
                                <option value="">Selecione o objetivo</option>
                                @foreach ($objetivos as $objetivo)
                                    <option value="{{ $objetivo->objetivo }}" <?php echo ($objetivo_ficha == $objetivo->objetivo) ? "selected" : "" ?> > 
                                            {{ $objetivo->objetivo }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                       
                        <div class="field" id="selectTreino">
                            <label for="Ficha_objetivo">Treino:</label>
                            <select name="Treino[]" id="treino" class="ui fluid search dropdown" >
                                <option value="">Selecione o treino</option>
                                  @foreach ($treinos as $treino)
                                    <option value="{{ $treino->treino_tipo }}" <?php echo ($fichaExercicio->treino_ficha == $treino->treino_tipo) ? "selected" : "" ?> > 
                                        {{ $treino->treino_tipo }}
                                    </option>
                                 @endforeach
                            </select>
                        </div>
                </div>

                <div id="conteudo">
                    <div class="six fields">
                        <select name="" id="selectPrincipal" class="ui fluid dropdown">
                            @foreach ($exercicios as $exercicio)
                                <option value="{{ $exercicio->id_exercicio }}" <?php echo ($fichaExercicio->id_forekey_exercicio == $exercicio->id_exercicio) ? "selected" : " " ?> > 
                                {{ $exercicio->nome_exercicio }}
                                </option>
                            @endforeach
                        </select>   

                        <input type="number" name="Repeticao[{{ $fichaExercicio->treino_ficha }}][]" value="{{ $fichaExercicio->qtd_repeticoes_ficha }}">
                        
                        <input type="number" name="Serie[{{ $fichaExercicio->treino_ficha}} ][]" value="{{ $fichaExercicio->qtd_serie_ficha }}">
                        
                        <input type="number" name="Descanso[{{ $fichaExercicio->treino_ficha}} ][]" value="{{ $fichaExercicio->tempo_descanco_ficha }}">
                        <input type="text" name="Imagem[{{ $fichaExercicio->imagem_exercicio}} ][]" value="{{ $fichaExercicio->imagem_exercicio }}">
                    </div>
                </div>

                <div class="field">
                    <button type="submit" class="ui green button" name="save">Finalizar
                    </button>
                </div>
                
            </div>
        </form>
    </div>

@endsection