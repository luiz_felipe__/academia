@extends('modules.fichas.module')

@section('module-content')

    <div class="ui container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
    
            <div class="ui segment">    
                <h4 class="ui header" style="padding-top: 8px">Criar Ficha de Treino</h4>
                <div class="ui divider" style="width: 12%"></div>
                <div class="two fields">
                    <div class="field">
                        <label for="">Data de início da ficha</label>
                        <input type="date" name="dataInicio[inicio]" id="ficha_name" required>
                    </div>
                    <div class="field">
                        <label for="">Data de término da ficha</label>
                        <input type="date" name="dataTermino[termino]" id="ficha_name" required>
                    </div>
                </div>
                <br>     
                
                <div class="two fields">
                        <div class="field">
                            <label for="ficha_objetivo">Objetivo:</label>
                            <select name="Objetivo[objetivo_ficha]" class="ui dropdown" required>
                                <option value="">Selecione o Objetivo</option>
                                 @foreach ($objetivos as $objetivo)
                                    <option value="{{ $objetivo->id_objetivo }}">{{ $objetivo->objetivo }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field" id="selectTreino">
                            <label for="Ficha_objetivo">Treino:</label>
                            <select name="Treino[]" id="treino" class="ui fluid search dropdown" required>
                                <option value="">Selecione o treino</option>
                                @foreach ($treinos as $treino)
                                    <option value="{{ $treino->id_treino }}">{{ $treino->treino_tipo }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>Ação</label>
                            <a class="ui button" onclick="addDiv()">Adicionar</a>         
                        </div>
                </div>

                <div id="array_exercicio" style="display: none">
                    <select name="" id="selectPrincipal" class="ui fluid dropdown">
                        <option value="">Selecione o exercicio</option>
                        @foreach ($exercicios as $exercicio)
                            <option value="{{ $exercicio->id_exercicio }}">{{ $exercicio->nome_exercicio }}</option>
                        @endforeach
                    </select>        
                </div>


                <div id="conteudo">
                
                </div> 
                
                <div class="field">
                    <button class="ui green button">Finalizar</button>
                </div>
            </div>  
        </form>
    </div>
@endsection