@extends('modules.series.module')

@section('module-content')

    <div class="container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
            
            <div class="ui segment">
                <h4 class="ui header">Criar Série:</h4>
                <div class="field">
                    <div class="two fields">
                        <div class="field">
                            <label for="serie_name">Número de Série:</label>
                            <input type="number" id="serie_name" name="serie[qtd_serie_serie]" value="{{($type == 'create' ? old('serie.qtd_serie_serie') : $serie->qtd_serie_serie)}}" placeholder="First Name"  required>
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="field">
                            <label for="serie_name">Quantidade de repetições:</label>
                            <input type="number" id="serie_name" name="serie[qtd_repeticoes_serie]" value="{{($type == 'create' ? old('serie.qtd_repeticoes_serie') : $serie->qtd_repeticoes_serie)}}" placeholder="First Name"  required>
                        </div>
                    </div>
                    <div class="field">
                        <button type="submit" class="ui green button" name="save">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection