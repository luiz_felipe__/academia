@extends('modules.series.module')

@section('module-content')
    
    <div class="container search">
        <form action="{{route('serie.index')}}" method="get" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}
            <div class="ui icon input">
              <input type="text" name="serie_search" placeholder="Search...">
              <i class="circular search link icon"></i>
            </div>
        </form>
    </div>   

    @if (!is_null($rows))
        <div class="ui grid">
            <div class="twelve wide column">
                <table class="ui single line celled table"">
                    <thead>
                    <tr>
                        <th>Repetições</th>
                        <th>Séries</td>
                        <th width="160">Ações</td>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach ($rows as $row)
                        <tr>
                            <td>
                                <div class="four wide column">{{$row->qtd_repeticoes_serie}}</div>
                            </td>
                            <td>
                                <div class="four wide column">{{$row->qtd_serie_serie}}</div>
                            </td>
                            <td>
                                <form action="{{route('serie.destroy', $row->id_serie) }}" method="post" class="ui form" onsubmit="return deleteData();">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <a href="{{route('serie.edit', $row->id_serie ) }}" class="ui blue icon button" data-tooltip="Editar"><i class="icon edit"></i></a>
                                    <button class="ui red icon button btn-remove" data-tooltip="Remover"><i class="icon trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        <p>Nenhum registro encontrado.</p>
    @endif

@endsection