@extends('layouts.dashboard')

@section('content-wizard')

    <div class="action-buttons">
        <a href="{{route('serie.index')}}" class="ui button blue">Listar series</a>
        <a href="{{route('serie.create')}}" class="ui button blue">Nova serie</a>
    </div>
    
    @yield('module-content')

@endsection