@extends('modules.grupo_muscular.module')

@section('module-content')

    <div class="container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
            
            <div class="ui segment">
                <h4 class="ui header">Criar Grupo muscular:</h4>
                <div class="field">
                    <div class="two fields">
                        <div class="field">
                            <label for="grupo_muscular_name">Exercício:</label>
                            <input type="number" id="grupo_muscular_name" name="grupo_muscular[id_exercicio]" value="{{($type == 'create' ? old('grupo_muscular.id_exercicio') : $grupo_muscular->id_exercicio)}}" placeholder="First Name"  required>
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="field">
                            <label for="grupo_muscular_name">Músculo</label>
                            <input type="number" id="grupo_muscular_name" name="grupo_muscular[id_musculo]" value="{{($type == 'create' ? old('grupo_muscular.id_musculo') : $grupo_muscular->id_musculo)}}" placeholder="First Name"  required>
                        </div>
                    </div>
                    <div class="field">
                        <button type="submit" class="ui green button" name="save">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection