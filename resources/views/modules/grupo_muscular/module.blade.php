@extends('layouts.dashboard')

@section('content-wizard')

    <div class="action-buttons">
        <a href="{{route('grupo_muscular.index')}}" class="ui button">Listar grupo musculares</a>
        <a href="{{route('grupo_muscular.create')}}" class="ui button">Novo grupo muscular</a>
    </div>
    
    @yield('module-content')

@endsection