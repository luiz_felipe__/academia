@extends('modules.grupo_muscular.module')

@section('module-content')
    
    <div class="container search">
        <form action="#" method="get" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}
            <div class="ui segment">
                <h4 class="ui header">Termo de pesquisa</h4>
                    <div class="ui input">
                      <input type="text" name="category_search" placeholder="Search...">
                    </div>
                     <button class="ui blue button"><i class="search icon"></i></button>
            </div>
        </form>
    </div>   

    @if (!is_null($rows))
        <div class="ui grid">
            <div class="twelve wide column">
                <table class="ui table">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th width="160">Ações</td>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach ($rows as $row)
                        <tr>
                            <td>
                                <div class="four wide column">{{$row->nome}}</div>
                            </td>
                            <td>
                                <form action="#" method="post" class="ui form" onsubmit="return deleteData();">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <a href="#" class="ui blue icon button" data-tooltip="Editar"><i class="icon edit"></i></a>
                                    <button class="ui red icon button btn-remove" data-tooltip="Remover"><i class="icon trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        <p>Nenhum registro encontrado.</p>
    @endif

@endsection