@extends('modules.musculos.module')

@section('module-content')

    <div class="container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
            
            <div class="ui segment">
                <h4 class="ui header">Criar categoria de musculo:</h4>
                <div class="field">
                    <div class="two fields">
                        <div class="field">
                            <label for="musculo_name">Nome do musculo:</label>
                            <input type="text" id="musculo_name" name="musculo[nome_musculo]" value="{{($type == 'create' ? old('musculo.nome_musculo') : $musculo->nome_musculo)}}" placeholder="First Name"  required>
                        </div>
                    </div>
                    <div class="field">
                        <button type="submit" class="ui green button" name="save">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection