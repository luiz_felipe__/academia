@extends('modules.musculos.module')

@section('module-content')
    
    <div class="container search">
        <form action="{{route('musculo.index')}}" method="get" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}
            <div class="ui icon input">
              <input type="text" name="musculo_search" placeholder="Search...">
              <i class="circular search link icon"></i>
            </div>
        </form>
    </div>   

    @if (!is_null($data))
        <div class="twelve wide column">
            <table class="ui single line celled table"">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th width="160">Ações</td>
                </tr>
                </thead>
                <tbody>

                    @foreach ($data as $row)
                    <tr>
                        <td>
                            <div class="four wide column">{{$row->nome_musculo}}</div>
                        </td>
                        <td>
                            <form action="{{route('musculo.destroy', $row->id_musculo) }}" method="post" class="ui form" onsubmit="return deleteData();">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <a href="{{route('musculo.edit', $row->id_musculo ) }}" class="ui blue icon button" data-tooltip="Editar"><i class="icon edit"></i></a>
                                <button class="ui red icon button btn-remove" data-tooltip="Remover"><i class="icon trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
        <div>
            @include('layouts.pagination')
        </div>

    @else
        <p>Nenhum registro encontrado.</p>
    @endif

@endsection