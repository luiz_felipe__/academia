@extends('layouts.dashboard')

@section('content-wizard')

    <div class="action-buttons">
        <a href="{{route('musculo.index')}}" class="ui button teal">Listar musculos</a>
        <a href="{{route('musculo.create')}}" class="ui button teal">Novo musculo</a>
    	@yield('module-content')
    </div>

@endsection