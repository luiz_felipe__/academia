@extends('modules.medidas.module')

@section('module-content')

    <div class="container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
            
            <div class="ui segment">
                <h4 class="ui header">Inserir Medidas</h4>
                    <div class="two fields"> 
                        <div class="field">
                            <label for="medidas_data_medicao_medida">Data da Medição</label>
                            <input type="date" name="Medida[data_medicao_medida]" value="{{($type == 'create' ? old('Medida.data_medicao_medida') : $medida->data_medicao_medida)}}" placeholder="Medida" >
                        </div>

                        <div class="field">
                            <label for="medidas_name">Peso</label>
                            <div class="ui right labeled input">
                              <input type="text" class="input_medidas" name="Medida[peso_medida]" value="{{($type == 'create' ? old('Medida.peso_medida') : $medida->peso_medida)}}" placeholder="Peso" required>
                              <div class="ui basic label">
                                kg
                              </div>
                            </div>
                        </div>
                    </div>

                    <div class="two fields">    
                        <div class="field">
                            <label for="medidas_name">Peito</label>
                            <div class="ui right labeled input">
                              <input type="text" class="input_medidas" name="Medida[peito_medida]" value="{{($type == 'create' ? old('Medida.peito_medida') : $medida->peito_medida)}}" placeholder="Peito" required>
                              <div class="ui basic label">
                                cm
                              </div>
                            </div>
                        </div>

                        <div class="field">
                            <label for="medidas_name">Antebraço</label>
                            <div class="ui right labeled input">
                              <input type="text" class="input_medidas" name="Medida[ante_braco_medida]" value="{{($type == 'create' ? old('Medida.ante_braco_medida') : $medida->ante_braco_medida)}}" placeholder="Antebraço" required>
                              <div class="ui basic label">
                                cm
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="two fields">        
                        <div class="field">
                            <label for="medidas_name">Braço</label>
                            <div class="ui right labeled input">
                              <input type="text" class="input_medidas" name="Medida[braco_medida]" value="{{($type == 'create' ? old('Medida.braco_medida') : $medida->braco_medida)}}" placeholder="Braço" required>
                              <div class="ui basic label">
                                cm
                              </div>
                            </div>

                            
                        </div>

                        <div class="field">
                            <label for="medidas_name">Perna</label>
                            <div class="ui right labeled input">
                              <input type="text" class="input_medidas" name="Medida[perna_medida]" value="{{($type == 'create' ? old('Medida.perna_medida') : $medida->perna_medida)}}" placeholder="Perna" required>
                              <div class="ui basic label">
                                cm
                              </div>
                            </div>

                            
                        </div>
                    </div>            
                    <div class="two fields">
                        <div class="field">
                            <label for="medidas_name">Quadril</label>
                            <div class="ui right labeled input">
                              <input type="text" class="input_medidas" name="Medida[quadris_medida]" value="{{($type == 'create' ? old('Medida.quadris_medida') : $medida->quadris_medida)}}" placeholder="Quadril" required>
                              <div class="ui basic label">
                                cm
                              </div>
                            </div>

                            
                        </div>

                        <div class="field">
                            <label for="medidas_name">Panturrilha</label>
                            <div class="ui right labeled input">
                              <input type="text" class="input_medidas" name="Medida[panturrilha_medida]" value="{{($type == 'create' ? old('Medida.panturrilha_medida') : $medida->panturrilha_medida)}}" placeholder="Panturrilha" required>
                              <div class="ui basic label">
                                cm
                              </div>
                            </div>

                            
                        </div>
                    </div>        
                    <div class="two field">
                        <br>
                        <button type="submit" class="ui green button" name="save">Salvar</button>
                    </div>
                        
            </div>
        </form>
    </div>

@endsection