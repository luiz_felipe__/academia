@extends('layouts.internal')

@section('content-wizard-internal')

    <div class="container">
        <a href="{{route('cliente.medida.index', $cliente->id_cliente)}}" class="ui button teal">Listar medidas</a>
        <a href="{{route('cliente.medida.create', $cliente->id_cliente)}}" class="ui button teal">Nova medida</a>
        @yield('module-content')
    </div>

@endsection