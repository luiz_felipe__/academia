@extends('modules.medidas.module')

@section('module-content')
    
    <div class="container search">
        <form action="{{route('cliente.medida.index',$cliente->id_cliente)}}" method="get" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}
            <div class="ui icon input">
                <input type="text" name="medida_search" placeholder="Search...">
                <i class="circular search link icon"></i>
            </div>
        </form>
    </div>
    <br>   

    @if ($data)
        <div class="twelve wide column">
            <table class="ui single line celled table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Data da medição</th>
                    <th>Peso</th>
                    <th>Peito</th>
                    <th>Ante braço</th>
                    <th>Braço</th>        
                    <th>Perna</th>
                    <th>Quahris</th>
                    <th>Panturrilha</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>

                    @foreach ($data as $row)
                    <tr>
                        <td>
                            {{$row->id_medida}} 
                        </td>
                        <td>{{$row->data_medicao_medida}}</td>
                        <td>{{$row->peso_medida}} Kg</td>
                        <td>{{$row->peito_medida}} cm</td>
                        <td>{{$row->ante_braco_medida}} cm</td>
                        <td>{{$row->braco_medida}} cm</td>
                        <td>{{$row->perna_medida}} cm</td>
                        <td>{{$row->quadris_medida}} cm</td>
                        <td>{{$row->panturrilha_medida}} cm</td>

                        <td>
                            <form action="#" method="post" class="ui form" onsubmit="return deleteData();">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                 <a href="{{route('cliente.medida.edit', [ $cliente->id_cliente, $row->id_medida ]) }}" class="ui blue icon button" data-tooltip="Editar"><i class="icon edit"></i></a>
                                <button class="ui red icon button btn-remove" data-tooltip="Remover"><i class="icon trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            @include('layouts.pagination')
        </div>
        
    @else
        <p>Nenhum registro encontrado.</p>
    @endif

@endsection