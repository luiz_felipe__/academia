@extends('layouts.dashboard')

@section('content-wizard')

    <div class="container">
        <a href="{{route('exercicio.index')}}" class="ui button teal">Listar Exercícios</a>
        <a href="{{route('exercicio.create')}}" class="ui button teal">Novo Exercício</a>
        @yield('module-content')
    </div>

@endsection