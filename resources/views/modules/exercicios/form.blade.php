@extends('modules.exercicios.module')

@section('module-content')

    <div class="container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
            
            <div class="ui segment">
                <h4 class="ui header">Criar exercício</h4>
                <div class="field">
                    <div class="two fields">
                        <div class="field">
                            <label for="exercicio_name">Nome do exercício:</label>
                            <input type="text" id="exercicio_name" name="exercicio[nome_exercicio]" value="{{($type == 'create' ? old('exercicio.nome_exercicio') : $exercicio->nome_exercicio)}}" placeholder="First Name"  required>
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="field">
                            <label for="exercicio_matricula">Exercício alternativo:</label>
                            <input type="text" id="exercicio_matricula" name="exercicio[alternativo_exercicio]" value="{{($type == 'create' ? old('exercicio.alternativo_exercicio') : $exercicio->alternativo_exercicio)}}" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="field">
                        <button type="submit" class="ui green button" name="save">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection