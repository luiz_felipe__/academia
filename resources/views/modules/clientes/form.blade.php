@extends('modules.clientes.module')
@section('module-content')

    <div class="container">
        <form  action="{{$action}}" method="post" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}

            @if ($type == 'edit')
                {{method_field('PUT')}}
            @endif
            
            <div class="ui segment">
                <h4 class="ui header" style="padding-top: 8px">Criar Aluno</h4>
                <div class="ui divider" style="width: 10%"></div>
                <div class="field">
                    <div class="two fields">
                        <div class="field">
                            <label for="cliente_name">Nome:</label>
                            <input type="text" id="cliente_name" name="cliente[nome_cliente]" value="{{($type == 'create' ? old('cliente.nome_cliente') : $cliente->nome_cliente)}}" placeholder="Nome Completo" >
                        </div>
                        <div class="field">
                            <label for="cliente_matricula">Matrícula:</label>
                            <input type="text" id="cliente_matricula" name="cliente[matricula_cliente]" value="{{($type == 'create' ? old('cliente.matricula_cliente') : $cliente->matricula_cliente)}}" placeholder="Matrícula">
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="field">
                            <label for="cliente_name">E-mail:</label>
                            <input type="text" id="cliente_email" name="cliente[email_cliente]" value="{{($type == 'create' ? old('cliente.email_cliente') : $cliente->email_cliente)}}" placeholder="E-mail" >
                        </div>
                        <div class="field">
                            <label for="cliente_matricula">Senha:</label>
                            <input type="password" id="cliente_senha" name="cliente[senha_cliente]" value="{{($type == 'create' ? old('cliente.senha_cliente') : $cliente->senha_cliente)}}" placeholder="Senha">
                        </div>
                    </div>
                    <div class="field">
                        <button type="submit" class="ui green button" name="save">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection