@extends('layouts.dashboard')
@section('content-wizard')

    <div class="container">
        <a href="{{route('cliente.index')}}" class="ui button teal">Listar Clientes</a>
        <a href="{{route('cliente.create')}}" class="ui button teal">Novo Cliente</a>
    	@yield('module-content')
    </div>
    
@endsection