@extends('modules.clientes.module')

@section('module-content')
    
    <div class="container search">
        <form action="{{route('cliente.index')}}" method="get" enctype="multipart/form-data" class="ui form">
            {!! csrf_field() !!}
            <div class="ui icon input">
              <input type="text" class="search" name="cliente_search" placeholder="Pesquisar cliente...">
              <i class="circular search link icon"></i>
            </div>
        </form>
    </div>
 
    @if (!is_null($data))
        
            <div class="twelve wide column">
                <table class="ui single line celled table"">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Matrícula</th>
                        <th width="160">Ações</td>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach ($data as $row)
                        <tr>
                            <td>
                                <div class="four wide column"> {{$row->id_cliente}}</div>
                            </td>
                            <td>
                                <div class="four wide column"> {{$row->nome_cliente}}</div>
                            </td>
                            <td>
                                <div class="four wide column"> {{$row->matricula_cliente}}</div>
                            </td>
                            <td>
                                <form action="{{route('cliente.destroy', $row->id_cliente) }}" method="post" class="ui form" onsubmit="return deleteData();"> 
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <a href="{{ route('cliente.ficha.index', $row->id_cliente) }}" class="ui teal icon button" data-tooltip="Gerenciar"><i class="icon cogs"></i></a>
                                    <a href="{{route('cliente.edit', $row->id_cliente ) }}" class="ui blue icon button" data-tooltip="Editar"><i class="icon edit"></i></a>
                                    <button class="ui red icon button btn-remove" data-tooltip="Remover"><i class="icon trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


        <div>
            @include('layouts.pagination')
        </div>

    @else
        <p>Nenhum registro encontrado.</p>
    @endif

    <div class="ui basic modal">
      <div class="ui icon header">
        <i class="archive icon"></i>
        Deseja realmente deletar essas informações?
      </div>
      <div class="actions">
        <div id="cancel" class="ui red basic cancel inverted button">
          <i class="remove icon"></i>
          NÃO
        </div>
        <div id="ok" class="ui green ok inverted button">
          <i class="checkmark icon"></i>
          SIM
        </div>
      </div>
    </div>    

@endsection